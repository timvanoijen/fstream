package nl.timvanoijen.fstream.collectors;

import nl.timvanoijen.fstream.FStream;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumberCollectorTest {

    @Test
    void testIntegerSum() {
        var src = FStream.of(1, 2, 3);
        var result = src.mapToInt().sum();
        assertEquals(6, result);
    }

    @Test
    void testIntegerProduct() {
        var src = FStream.of(2, 3, 4);
        var result = src.mapToInt().product();
        assertEquals(24, result);
    }
}
