package nl.timvanoijen.fstream.operators;

import nl.timvanoijen.fstream.FStream;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static nl.timvanoijen.fstream.TestUtils.assertFStreamEquals;


class FlatMapOperatorTest {

    @Test
    void test() {
        var src = FStream.of(1, 2, 3);
        var result = src.flatMapToInt(i -> Collections.nCopies(i, i));
        assertFStreamEquals(result, 1, 2, 2, 3, 3, 3);
    }

    @Test
    void testFlatMapWithEmptyIterableStart() {
        var src = FStream.of(0, 1, 2 );
        var result = src.flatMap(i -> Collections.nCopies(i, i));
        assertFStreamEquals(result, 1, 2, 2);
    }

    @Test
    void testFlatMapWithEmptyIterableMiddle() {
        var src = FStream.of(1, 0, 2 );
        var result = src.flatMapToLong(i -> Collections.nCopies(i, Long.valueOf(i)));
        assertFStreamEquals(result, 1L, 2L, 2L);
    }

    @Test
    void testFlatMapWithEmptyIterableEnd() {
        var src = FStream.of(1, 2, 0);
        var result = src.flatMapToInt(i -> Collections.nCopies(i, i));
        assertFStreamEquals(result, 1, 2, 2);
    }
}