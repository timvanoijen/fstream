package nl.timvanoijen.fstream.operators;

import nl.timvanoijen.fstream.FStream;
import nl.timvanoijen.fstream.streams.GenericFStream;
import org.junit.jupiter.api.Test;

import static nl.timvanoijen.fstream.TestUtils.assertFStreamEquals;


class MapOperatorTest {

    @Test
    void test() {
        var src = FStream.of("1", "2", "3");
        var result = src.map(Integer::parseInt);
        assertFStreamEquals(result, 1, 2, 3);
    }

    @Test
    void testMapFunctionInputSuperclass() {
        var src = FStream.of("1", "2", "3");
        var result = src.map(Object::getClass);
        assertFStreamEquals(result, String.class, String.class, String.class);
    }

    @Test
    void testMapFunctionOutputSubclass() {
        var src = FStream.of("1", "2", "3");
        GenericFStream<Number> result = src.map(Integer::parseInt);
        assertFStreamEquals(result, 1, 2, 3);
    }

    @Test
    void testMapToInt() {
        var src = FStream.of("1", "2", "3");
        var result = src.mapToInt(Integer::parseInt);
        assertFStreamEquals(result, 1, 2, 3);
    }

    @Test
    void testMapToLong() {
        var src = FStream.of("1", "2", "3");
        var result = src.mapToLong(Long::parseLong);
        assertFStreamEquals(result, 1L, 2L, 3L);
    }
}