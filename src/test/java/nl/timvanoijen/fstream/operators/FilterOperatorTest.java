package nl.timvanoijen.fstream.operators;

import nl.timvanoijen.fstream.FStream;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static nl.timvanoijen.fstream.TestUtils.assertFStreamEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


class FilterOperatorTest {

    @Test
    void testFilterOutEnd() {
        var src = FStream.of(1, 2, 3);
        var result = src.filter(n -> n < 3);
        assertFStreamEquals(result, 1, 2);
    }

    @Test
    void testFilterOutStart() {
        var src = FStream.of(1, 2, 3);
        var result = src.filter(n -> n > 2);
        assertFStreamEquals(result, 3);
    }

    @Test
    void testFilterOutMiddle() {
        var src = FStream.of(1, 2, 3);
        var result = src.filter(n -> n != 2);
        assertFStreamEquals(result, 1, 3);
    }

    @Test
    void testAskHasNextMultipleTimes() {
        var src = FStream.of(1, 2, 3, 4, 5);
        var result = src.filter(n -> n == 2 || n == 4);
        var it = result.iterator();
        var result2 = new ArrayList<Integer>();
        while(it.hasNext()) {
            it.hasNext();
            result2.add(it.next());
            it.hasNext();
        }
        assertEquals(Arrays.asList(2, 4), result2);
    }
}