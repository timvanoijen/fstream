package nl.timvanoijen.fstream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUtils {
    public static void assertFStreamEquals(FStream<?, ?> s, Object... values) {
        assertEquals(FStream.of(values).toList(), s.toList());
    }
}
