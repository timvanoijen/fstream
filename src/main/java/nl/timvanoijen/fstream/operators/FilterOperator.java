package nl.timvanoijen.fstream.operators;

import nl.timvanoijen.fstream.FStream;
import nl.timvanoijen.fstream.helpers.NaiveIterator;

import java.util.Iterator;
import java.util.Optional;
import java.util.function.Predicate;

public final class FilterOperator<S extends FStream<S, T>, T> extends TypeInvariantElementOperator<S, T> {

    private final Predicate<? super T> filter;

    public FilterOperator(Predicate<? super T> filter) {
        this.filter = filter;
    }

    @Override
    public Iterator<T> operate(Iterator<T> input) {
        return new InnerIterator<>(input, filter);
    }

    private static final class InnerIterator<T> extends NaiveIterator<T> {

        private final Iterator<T> input;
        private final Predicate<? super T> filter;

        public InnerIterator(Iterator<T> input,
                             Predicate<? super T> filter) {
            this.input = input;
            this.filter = filter;
        }

        @Override
        protected Optional<T> tryGetNext() {
            while (input.hasNext()) {
                T cur = input.next();
                if (filter.test(cur))
                    return Optional.ofNullable(cur);
            }
            return Optional.empty();
        }
    }
}
