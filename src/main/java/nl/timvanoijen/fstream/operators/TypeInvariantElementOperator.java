package nl.timvanoijen.fstream.operators;

import nl.timvanoijen.fstream.FStream;

import java.util.Iterator;

public abstract class TypeInvariantElementOperator<S extends FStream<S,T>, T> implements Operator<S, T, S, T> {
    public S operate(S input) {
        Iterable<T> it = () -> operate(input.iterator());
        return input.cloneWithIterable(it);
    }

    public abstract Iterator<T> operate(Iterator<T> input);
}
