package nl.timvanoijen.fstream.operators;

import nl.timvanoijen.fstream.FStream;
import nl.timvanoijen.fstream.helpers.DowncastIterable;
import nl.timvanoijen.fstream.helpers.NaiveIterator;
import nl.timvanoijen.fstream.streams.GenericFStream;

import java.util.Iterator;
import java.util.Optional;
import java.util.function.Function;

public final class FlatMapOperator<S1 extends FStream<S1, T1>, T1, T2> extends TypeVariantElementOperator<S1, T1, T2> {

    private final Function<? super T1, ? extends Iterable<? extends T2>> mapper;

    public FlatMapOperator(Function<? super T1, ? extends Iterable<? extends T2>> mapper) {
        this.mapper = mapper;
    }

    @Override
    public GenericFStream<T2> operate(S1 input) {
        return new GenericFStream<>(() -> new InnerIterator<>(input.iterator(), mapper));
    }

    @Override
    public Iterator<T2> operate(Iterator<T1> input) {
        return new InnerIterator<>(input, mapper);
    }

    private static final class InnerIterator<T1, T2> extends NaiveIterator<T2> {

        private final Iterator<T1> input;
        private final Function<? super T1, ? extends Iterable<? extends T2>> mapper;
        private Iterator<T2> innerIterator = null;

        public InnerIterator(Iterator<T1> input,
                             Function<? super T1, ? extends Iterable<? extends T2>> mapper) {
            this.input = input;
            this.mapper = mapper;
        }

        @Override
        protected Optional<T2> tryGetNext() {
            if (innerIterator == null) {
                if (!input.hasNext())
                    return Optional.empty();
                innerIterator = new DowncastIterable<T2>(mapper.apply(input.next())).iterator();
            }

            if (!innerIterator.hasNext()) {
                innerIterator = null;
                return tryGetNext();
            }
            return Optional.ofNullable(innerIterator.next());
        }
    }
}
