package nl.timvanoijen.fstream.operators;

import nl.timvanoijen.fstream.FStream;
import nl.timvanoijen.fstream.streams.GenericFStream;

import java.util.Iterator;
import java.util.function.Function;

public final class MapOperator<S1 extends FStream<S1, T1>, T1, T2> extends TypeVariantElementOperator<S1, T1, T2> {

    private final Function<? super T1, ? extends T2> mapper;

    public MapOperator(Function<? super T1, ? extends T2> mapper) {
        this.mapper = mapper;
    }

    @Override
    public GenericFStream<T2> operate(S1 input) {
        return new GenericFStream<>(() -> new InnerIterator<>(input.iterator(), mapper));
    }

    @Override
    public Iterator<T2> operate(Iterator<T1> input) {
        return new InnerIterator<>(input, mapper);
    }

    private static final class InnerIterator<T1, T2> implements Iterator<T2> {

        private final Iterator<T1> input;
        private final Function<? super T1, ? extends T2> mapper;

        public InnerIterator(Iterator<T1> input,
                             Function<? super T1, ? extends T2> mapper) {
            this.input = input;
            this.mapper = mapper;
        }

        @Override
        public boolean hasNext() {
            return input.hasNext();
        }

        @Override
        public T2 next() {
            return mapper.apply(input.next());
        }
    }
}
