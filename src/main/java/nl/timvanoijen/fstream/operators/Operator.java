package nl.timvanoijen.fstream.operators;

import nl.timvanoijen.fstream.FStream;

@FunctionalInterface
public interface Operator<S1 extends FStream<S1, T1>, T1, S2 extends FStream<S2, T2>, T2> {
    S2 operate(S1 input);
}
