package nl.timvanoijen.fstream.operators;

import nl.timvanoijen.fstream.FStream;
import nl.timvanoijen.fstream.streams.GenericFStream;

import java.util.Iterator;

public abstract class TypeVariantElementOperator<S1 extends FStream<S1,T1>, T1, T2>
        implements Operator<S1, T1, GenericFStream<T2>, T2> {

    public GenericFStream<T2> operate(S1 input) {
        Iterable<T2> it = () -> operate(input.iterator());
        return new GenericFStream<>(it);
    }

    public abstract Iterator<T2> operate(Iterator<T1> input);
}
