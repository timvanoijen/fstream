package nl.timvanoijen.fstream.sources;

import nl.timvanoijen.fstream.streams.GenericFStream;

import java.util.Arrays;

public final class ArrayFStream<T> extends GenericFStream<T> {
    public ArrayFStream(T[] arr) {
        super(Arrays.asList(arr));
    }
}
