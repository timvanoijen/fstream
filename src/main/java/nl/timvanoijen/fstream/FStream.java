package nl.timvanoijen.fstream;

import nl.timvanoijen.fstream.collectors.CollectionCollector;
import nl.timvanoijen.fstream.collectors.Collector;
import nl.timvanoijen.fstream.helpers.GenericCastUtils;
import nl.timvanoijen.fstream.operators.FilterOperator;
import nl.timvanoijen.fstream.operators.FlatMapOperator;
import nl.timvanoijen.fstream.operators.MapOperator;
import nl.timvanoijen.fstream.sources.ArrayFStream;
import nl.timvanoijen.fstream.streams.GenericFStream;
import nl.timvanoijen.fstream.streams.numbers.IntFStream;
import nl.timvanoijen.fstream.streams.numbers.LongFStream;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public interface FStream<S extends FStream<S, T>, T> extends Iterable<T> {

    /*
     * Java self-idiom to support different types of streams whose type can be maintained by operations
     * defined on the base FStream type (like filtering).
     */

    S cloneWithIterable(Iterable<T> iterable);

    S self();

    /*
     * Sources
     */

    static <T> GenericFStream<T> of(Iterable<T> input) {
        return new GenericFStream<>(input);
    }

    @SafeVarargs
    static <T> GenericFStream<T> of(T... input) {
        return new ArrayFStream<>(input);
    }

    /*
     * Operators
     */

    default <T2> GenericFStream<T2> map(Function<? super T, ? extends T2> mapper) {
        return new MapOperator<S, T, T2>(mapper).operate(self());
    }

    default IntFStream mapToInt(Function<? super T, ? extends Integer> mapper) {
        GenericFStream<Integer> mapped = new MapOperator<S, T, Integer>(mapper).operate(self());
        return new IntFStream(mapped);
    }

    default IntFStream mapToInt() {
        return mapToInt(e -> GenericCastUtils.cast(e, Integer.class));
    }

    default LongFStream mapToLong(Function<? super T, ? extends Long> mapper) {
        GenericFStream<Long> mapped = new MapOperator<S, T, Long>(mapper).operate(self());
        return new LongFStream(mapped);
    }

    default LongFStream mapToLong() {
        return mapToLong(e -> GenericCastUtils.cast(e, Long.class));
    }

    default <T2> GenericFStream<T2> flatMap(Function<? super T, ? extends Iterable<? extends T2>> mapper) {
        return new FlatMapOperator<S, T, T2>(mapper).operate(self());
    }

    default <T2> IntFStream flatMapToInt(Function<? super T, ? extends Iterable<? extends Integer>> mapper) {
        GenericFStream<Integer> mapped = new FlatMapOperator<S, T, Integer>(mapper).operate(self());
        return new IntFStream(mapped);
    }

    default <T2> LongFStream flatMapToLong(Function<? super T, ? extends Iterable<? extends Long>> mapper) {
        GenericFStream<Long> mapped = new FlatMapOperator<S, T, Long>(mapper).operate(self());
        return new LongFStream(mapped);
    }

    default S filter(Predicate<? super T> filter) {
        return new FilterOperator<S, T>(filter).operate(self());
    }

    /*
     * Collectors
     */

    default List<T> toList() {
        return new CollectionCollector<>(ArrayList<T>::new).collect(iterator());
    }

    default Set<T> toSet() {
        return new CollectionCollector<>(HashSet<T>::new).collect(iterator());
    }

    default <T2> T2 collect(Collector<T, ? extends T2> collector) {
        return collector.collect(iterator());
    }
}
