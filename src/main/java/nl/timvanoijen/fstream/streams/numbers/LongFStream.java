package nl.timvanoijen.fstream.streams.numbers;

import nl.timvanoijen.fstream.helpers.DowncastIterable;

public class LongFStream extends AbstractNumberFStream<LongFStream, Long> {

    public LongFStream(Iterable<? extends Long> it) {
        super(new DowncastIterable<>(it));
    }

    @Override
    protected Long one() {
        return 1L;
    }

    @Override
    protected Long zero() {
        return 0L;
    }

    @Override
    protected Long sum(Long a, Long b) {
        return a + b;
    }

    @Override
    protected Long product(Long a, Long b) {
        return a * b;
    }

    @Override
    public LongFStream cloneWithIterable(Iterable<Long> it) {
        return new LongFStream(it);
    }

    @Override
    public LongFStream self() {
        return this;
    }
}
