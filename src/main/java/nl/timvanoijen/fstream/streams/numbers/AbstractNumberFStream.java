package nl.timvanoijen.fstream.streams.numbers;

import nl.timvanoijen.fstream.collectors.ReduceCollector;
import nl.timvanoijen.fstream.streams.AbstractGenericFStream;


public abstract class AbstractNumberFStream<S extends AbstractNumberFStream<S, T>, T>
        extends AbstractGenericFStream<S, T> {

    public AbstractNumberFStream(Iterable<T> it) {
        super(it);
    }

    /*
     * Collectors
     */

    public final T sum() {
        return new ReduceCollector<T, T>(zero(), this::sum).collect(iterator());
    }

    public final T product() {
        return new ReduceCollector<T, T>(one(), this::product).collect(iterator());
    }

    protected abstract T one();

    protected abstract T zero();

    protected abstract T sum(T a, T b);

    protected abstract T product(T a, T b);
}
