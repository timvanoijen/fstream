package nl.timvanoijen.fstream.streams.numbers;

import nl.timvanoijen.fstream.helpers.DowncastIterable;

public class IntFStream extends AbstractNumberFStream<IntFStream, Integer> {

    public IntFStream(Iterable<? extends Integer> it) {
        super(new DowncastIterable<>(it));
    }

    @Override
    protected Integer one() {
        return 1;
    }

    @Override
    protected Integer zero() {
        return 0;
    }

    @Override
    protected Integer sum(Integer a, Integer b) {
        return a + b;
    }

    @Override
    protected Integer product(Integer a, Integer b) {
        return a * b;
    }

    @Override
    public IntFStream cloneWithIterable(Iterable<Integer> it) {
        return new IntFStream(it);
    }

    @Override
    public IntFStream self() {
        return this;
    }
}
