package nl.timvanoijen.fstream.streams;

import nl.timvanoijen.fstream.FStream;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public abstract class AbstractGenericFStream<S extends AbstractGenericFStream<S, T>, T> implements FStream<S, T> {

    private final Iterable<T> it;

    public AbstractGenericFStream(Iterable<T> it) {
        this.it = it;
    }

    @Override
    public Iterator<T> iterator() {
        return it.iterator();
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        it.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return it.spliterator();
    }
}
