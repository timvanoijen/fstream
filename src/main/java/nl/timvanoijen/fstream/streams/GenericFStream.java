package nl.timvanoijen.fstream.streams;


public class GenericFStream<T> extends AbstractGenericFStream<GenericFStream<T>, T> {

    public GenericFStream(Iterable<T> it) {
        super(it);
    }

    @Override
    public GenericFStream<T> cloneWithIterable(Iterable<T> iterable) {
        return new GenericFStream<>(iterable);
    }

    @Override
    public GenericFStream<T> self() {
        return this;
    }
}
