package nl.timvanoijen.fstream.helpers;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

public abstract class NaiveIterator<T> implements Iterator<T> {

    private T cachedNext = null;
    private Boolean cachedHasNext = null;

    @Override
    public boolean hasNext() {
        cacheNext();
        return cachedHasNext;
    }

    @Override
    public T next() {
        if (cachedHasNext == null)
            return tryGetNext().orElseThrow(NoSuchElementException::new);

        if (Boolean.FALSE.equals(cachedHasNext))
            throw new NoSuchElementException();

        T result = cachedNext;
        cachedNext = null;
        cachedHasNext = null;
        return result;
    }

    protected abstract Optional<T> tryGetNext();

    private void cacheNext() {
        if (cachedHasNext != null)
            return;

        var opt = tryGetNext();
        cachedHasNext = opt.isPresent();
        cachedNext = opt.orElse(null);
    }
}
