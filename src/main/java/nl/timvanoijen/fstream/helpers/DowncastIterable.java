package nl.timvanoijen.fstream.helpers;

import java.util.Iterator;

public class DowncastIterable<T> implements Iterable<T> {

    private final Iterable<? extends T> iterable;

    public DowncastIterable(Iterable<? extends T> iterable) {
        this.iterable = iterable;
    }

    @Override
    public Iterator<T> iterator() {
        var it = iterable.iterator();

        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public T next() {
                return it.next();
            }
        };
    }
}
