package nl.timvanoijen.fstream.helpers;


public final class GenericCastUtils {

    private GenericCastUtils() { throw new AssertionError(); }

    @SuppressWarnings("unchecked")
    public static <T> T cast(Object input, Class<T> toClazz) {
        if (input == null || toClazz.isAssignableFrom(input.getClass()))
            return (T)input;
        throw new IllegalArgumentException();
    }
}
