package nl.timvanoijen.fstream.collectors;

import java.util.Iterator;

@FunctionalInterface
public interface Collector<T1, T2> {
    T2 collect(Iterator<T1> iterator);
}
