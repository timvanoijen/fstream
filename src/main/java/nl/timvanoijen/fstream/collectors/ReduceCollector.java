package nl.timvanoijen.fstream.collectors;

import java.util.Iterator;
import java.util.function.BiFunction;

public class ReduceCollector<T1, T2> implements Collector<T1, T2> {

    private final T2 initialValue;
    private final BiFunction<? super T2, ? super T1, ? extends T2> reducer;

    public ReduceCollector(T2 initialValue, BiFunction<? super T2, ? super T1, ? extends T2> reducer) {
        this.initialValue = initialValue;
        this.reducer = reducer;
    }

    @Override
    public T2 collect(Iterator<T1> iterator) {
        T2 result = initialValue;
        while(iterator.hasNext())
            result = reducer.apply(result, iterator.next());
        return result;
    }
}
