package nl.timvanoijen.fstream.collectors;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Supplier;

public final class CollectionCollector<T1, T2 extends Collection<T1>> implements Collector<T1, T2> {

    private final Supplier<? extends T2> collectionSupplier;

    public CollectionCollector(Supplier<? extends T2> collectionSupplier) {
        this.collectionSupplier = collectionSupplier;
    }

    @Override
    public T2 collect(Iterator<T1> iterator) {
        T2 result = collectionSupplier.get();
        iterator.forEachRemaining(result::add);
        return result;
    }
}
